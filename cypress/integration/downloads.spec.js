

describe('Downloads', () => {
	beforeEach(() => {
		cy.visit('http://localhost:8998/#/home')
	})

	it('Download mp3', () => {
		cy.get('#mat-input-0')
		  .type('https://www.youtube.com/watch?v=CbYFdscgPOA')
		  .should('have.value', 'https://www.youtube.com/watch?v=CbYFdscgPOA')

		cy.get('#mat-checkbox-1-input').not('[disabled]')
      	  .check({ force: true }).should('be.checked')

		cy.get('button[type="submit"]:first')
          .should('be.visible').click()

		cy.wait(40000)

		cy.get('video')
	})

	it('Local download', () => {

		cy.get('app-unified-file-card:first')
		  .should('exist').click()

		cy.wait(3000)

		cy.url().should('include', 'player')

		cy.get('.row').within(() => {
			cy.get('button:first')
		  	  .should('be.visible').click()
		})
		
	})

	it('Delete video', () => {

		cy.get('app-unified-file-card:first').within(($card) => {
			cy.get('button:last')
			  .should('be.visible').click()
		})

		cy.get('.cdk-overlay-connected-position-bounding-box').within(($overlay) => {
			cy.get('button:last')
			  .should('be.visible').click()
		})

		cy.get('app-unified-file-card:first').should('not.exist')

	})

})
