FROM tzahi12345/youtubedl-material:nightly

COPY ./appdata/default.json ./appdata

RUN npm install

CMD ["npm start"]
